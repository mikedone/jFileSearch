package net.qqxh.resolve2view.impl;

import net.qqxh.common.utils.TxtCharsetUtil;
import net.qqxh.resolve2view.File2ViewResolve;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author 18060774
 */
@Component
public class Txt2ViewResolve implements File2ViewResolve {
    private static String RESOLVE_LIST = ".txt";
    private static String RESOLVE2FIX = "txt";


    @Override
    public String resolve(String fromPath,String toPath) throws IOException {
        try {
            TxtCharsetUtil.convertTextPlainFileCharsetToUtf8(fromPath, toPath);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
        return toPath;
    }

    @Override
    public boolean canResolve(String fileFix) {
        return RESOLVE_LIST.contains(fileFix.toLowerCase());

    }

    @Override
    public String getviewFix() {
        return RESOLVE2FIX;
    }



}
