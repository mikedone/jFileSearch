package net.qqxh.controller;

import net.qqxh.persistent.JfUserSimple;
import net.qqxh.service.SystemAnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jason on 2019/2/13 14:24
 *
 * @author jason
 */
@Controller
@RequestMapping("/systemAnalysis")
public class SystemAnalysisController extends BaseController {

    @Autowired
    private SystemAnalysisService systemAnalysisService;


    @RequestMapping("/getStatus")
    @ResponseBody
    public Object systemAnalysis() {
        Map data = new HashMap();
        JfUserSimple jfUserSimple = getLoginUser();
        if (jfUserSimple == null) {
            return responseJsonFactory.getSuccessJson("请登陆", "");
        }
        boolean esIsStart = systemAnalysisService.getEsIsStart();
        boolean redisIsStart = systemAnalysisService.getRedisIsStart();
        data.put("esIsStart", esIsStart);
        data.put("redisIsStart", redisIsStart);
        if(esIsStart){
            boolean systemIsInit=systemAnalysisService.getSystemIsInit(jfUserSimple.getSearchLib());
            data.put("systemIsInit", systemIsInit);
        }else {
            data.put("systemIsInit", false);
        }



        return responseJsonFactory.getSuccessJson("success", data);
    }

    /**
     * 查询系统是否格式化
     *
     * @return
     */
    public Object getInitState() {
        return null;
    }

    /**
     * 查询redis状态
     *
     * @return
     */
    public Object getRedisState() {
        return null;
    }


}
