package net.qqxh.service.autoRun;

import net.qqxh.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class AutoResolveFile implements ApplicationRunner {

    @Autowired
    FileService fileService;

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
       /* fileService.resolveAllFilee();*/
    }
}
